import matplotlib.pyplot as plt


def configure_ax(ax, values, title, max_y, min_y, max_h_line, min_h_line, unit):
    ax.plot(values)
    ax.set_title(title)
    ax.set_ylim(min_y, max_y)
    ax.set_ylabel(unit)
    ax.axhline(max_h_line, linestyle="--", color="#FF6666")
    ax.axhline(min_h_line, linestyle="--", color="#FF6666")
    ax.annotate(f'{values[-1]:.0f} {unit}', xy=(1, values[-1]), xytext=(8, 0),
                xycoords=('axes fraction', 'data'), textcoords='offset points')
    # print(values)


def show_figure(ef, pf, rthd):
    fig = plt.figure(figsize=(20, 20), dpi=100)
    fig.subplots_adjust(wspace=0.3, hspace=0.6)
    my_grid_layout = [(8, 2, (1, 3)), (8, 2, 2), (8, 2, 4), (8, 2, (5, 7)), (8, 2, (6, 8)), (8, 2, (9, 11)), (8, 2, 10),
                      (8, 2, 12), (8, 2, (13, 15)), (8, 2, 14), (8, 2, 16)]

    ax_hr = fig.add_subplot(*my_grid_layout[0])
    configure_ax(ax_hr, values=rthd[6], title="Heart Rate", max_y=230, min_y=30, max_h_line=220, min_h_line=40,
                 unit="bpm")

    ax_ef = fig.add_subplot(*my_grid_layout[1])
    ax_ef.text(0.1, 0.1,
               f'Environment Type: {"extreme" if ef[0] == 0 else "dangerous" if ef[0] == 1 else "hazardous"}\n'
               f'Outdoor Temperature: {ef[1]} °C\n'
               f'Location: {"Mountains" if ef[2] == 0 else "Desert"}\n'
               f'Altitude: {ef[3]} m', fontsize=18, ha='left')
    ax_ef.set_title("Environmental Quantities")
    ax_ef.set_xticks([])
    ax_ef.set_yticks([])

    ax_pf = fig.add_subplot(*my_grid_layout[2])
    ax_pf.text(0.1, 0.3, f'Age: {pf[0]}\n'
                         f'Gender: {"male" if pf[1] == 0 else "female" if pf[1] == 1 else "diverse"}\n'
                         f'Resting Heart Rate: {pf[2]} bpm', fontsize=18, ha='left')
    ax_pf.set_title("Physiological Fingerprint")
    ax_pf.set_xticks([])
    ax_pf.set_yticks([])

    ax_hrv = fig.add_subplot(*my_grid_layout[3])
    configure_ax(ax_hrv, values=rthd[7], title="Heart Rate Variability (RMSSD)", max_y=50, min_y=10, max_h_line=39,
                 min_h_line=15,
                 unit="ms")

    ax_core = fig.add_subplot(*my_grid_layout[4])
    configure_ax(ax_core, values=rthd[1], title="Core Body Temperature", max_y=45, min_y=30, max_h_line=42,
                 min_h_line=35.5,
                 unit="°C")

    ax_breath = fig.add_subplot(*my_grid_layout[5])
    configure_ax(ax_breath, values=rthd[0], title="Breathing Rate", max_y=40, min_y=0, max_h_line=35, min_h_line=6,
                 unit="bpm")

    ax_hand_r = fig.add_subplot(*my_grid_layout[6])
    configure_ax(ax_hand_r, values=rthd[2], title="Limb Temperature Right Wrist", max_y=40, min_y=25, max_h_line=34,
                 min_h_line=30.3,
                 unit="°C")

    ax_hand_l = fig.add_subplot(*my_grid_layout[7])
    configure_ax(ax_hand_l, values=rthd[3], title="Limb Temperature Left Wrist", max_y=40, min_y=25, max_h_line=34,
                 min_h_line=30.3,
                 unit="°C")

    ax_hum = fig.add_subplot(*my_grid_layout[8])
    configure_ax(ax_hum, values=rthd[8], title="Human Humidity", max_y=110, min_y=-10, max_h_line=100, min_h_line=0,
                 unit="%")

    ax_feet_r = fig.add_subplot(*my_grid_layout[9])
    configure_ax(ax_feet_r, values=rthd[4], title="Limb Temperature Right Ankle", max_y=40, min_y=23, max_h_line=32.7,
                 min_h_line=27.8,
                 unit="°C")

    ax_feet_l = fig.add_subplot(*my_grid_layout[10])
    configure_ax(ax_feet_l, values=rthd[5], title="Limb Temperature Left Ankle", max_y=40, min_y=23, max_h_line=32.7,
                 min_h_line=27.8,
                 unit="°C")

    return fig
