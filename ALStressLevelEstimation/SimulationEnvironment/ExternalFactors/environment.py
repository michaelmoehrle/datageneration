import numpy as np
from interface import implements
from collections import namedtuple

from .externalFactorsInterface import ExternalFactorInterface

ENV_TYPE_DICT = {'extreme': 0, 'dangerous': 1, 'hazardous': 2}
LOCATION_DICT = {'Mountain': 0, 'Desert': 1}

EnvType = namedtuple('EnvType', 'value min max')
EnvType.__new__.__defaults__ = (None, 0, max(ENV_TYPE_DICT.values()))
Temperature = namedtuple('Temperature', 'value min max')
Temperature.__new__.__defaults__ = (None, -30, 50)
Location = namedtuple('Location', 'value min max')
Location.__new__.__defaults__ = (None, 0, max(LOCATION_DICT.values()))
Altitude = namedtuple('Altitude', 'value min max')
Altitude.__new__.__defaults__ = (None, -11000, 8848)


class Environment(implements(ExternalFactorInterface)):
    def __init__(self, env_type, temperature, location, altitude):
        self._env_type = EnvType(ENV_TYPE_DICT[env_type])
        self._temperature = Temperature(temperature)
        self._location = Location(LOCATION_DICT[location])
        self._altitude = Altitude(altitude)

    @property
    def env_type(self):
        return self._env_type

    @property
    def temperature(self):
        return self._temperature

    @property
    def location(self):
        return self._location

    @property
    def altitude(self):
        return self._altitude

    def get_values(self):
        result = np.array([self.env_type.value, self.temperature.value, self.location.value, self.altitude.value])
        norm_result = np.array([self.normalise_value(self.env_type.value, self.env_type.min, self.env_type.max),
                                self.normalise_value(self.temperature.value, self.temperature.min, self.temperature.max),
                                self.normalise_value(self.location.value, self.location.min, self.location.max),
                                self.normalise_value(self.altitude.value, self.altitude.min, self.altitude.max)])

        return result, norm_result

    def normalise_value(self, value, minimum, maximum):
        norm_result = (value - minimum)/(maximum - minimum)
        return norm_result
