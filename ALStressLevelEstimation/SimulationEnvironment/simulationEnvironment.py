import logging

import numpy as np

from .ExternalFactors import ExternalFactorInterface
from .PhysiologicalFingerprint import PhysiologicalFingerprintInterface
from .RealTimeHealthData import RealTimeHealthDataInterface

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class SimulationEnviornment:
    def __init__(self, weights, external_factor, physiological_fingerprint, real_time_health_data):
        self.weights = weights
        self._external_factor = external_factor
        self._physiological_fingerprint = physiological_fingerprint
        self._real_time_healt_data = real_time_health_data

    @property
    def external_factor(self) -> ExternalFactorInterface:
        """
        The Context maintains a reference to one of the Strategy objects. The
        Context does not know the concrete class of a strategy. It should work
        with all strategies via the Strategy interface.
        """

        return self._external_factor

    @external_factor.setter
    def external_factor(self, external_factor: ExternalFactorInterface) -> None:
        """
        Usually, the Context allows replacing a Strategy object at runtime.
        """

        self._external_factor = external_factor

    @property
    def physiological_fingerprint(self) -> PhysiologicalFingerprintInterface:
        """
        The Context maintains a reference to one of the Strategy objects. The
        Context does not know the concrete class of a strategy. It should work
        with all strategies via the Strategy interface.
        """

        return self._physiological_fingerprint

    @physiological_fingerprint.setter
    def physiological_fingerprint(self, physiological_fingerprint: PhysiologicalFingerprintInterface) -> None:
        """
        Usually, the Context allows replacing a Strategy object at runtime.
        """

        self._physiological_fingerprint = physiological_fingerprint
        self._real_time_healt_data.update_persona_info(physiological_fingerprint.age, physiological_fingerprint.gender,
                                                       physiological_fingerprint.resting_heart_rate)

    @property
    def real_time_healt_data(self) -> RealTimeHealthDataInterface:
        """
        The Context maintains a reference to one of the Strategy objects. The
        Context does not know the concrete class of a strategy. It should work
        with all strategies via the Strategy interface.
        """

        return self._real_time_healt_data

    @real_time_healt_data.setter
    def real_time_healt_data(self, real_time_healt_data: RealTimeHealthDataInterface) -> None:
        """
        Usually, the Context allows replacing a Strategy object at runtime.
        """

        self._real_time_healt_data = real_time_healt_data

    def get_readings(self, amount):
        ef_values, ef_norm_values = self.external_factor.get_values()
        pf_values, pf_norm_values = self.physiological_fingerprint.get_values()
        rthd_values, rthd_norm_values = self.real_time_healt_data.generate_values(amount=amount)
        logger.debug(ef_values)
        logger.debug(pf_values)
        logger.debug(rthd_values)
        logger.debug(ef_norm_values)
        logger.debug(pf_norm_values)
        logger.debug(rthd_norm_values)
        # result = np.concatenate((ef_values, pf_values, rthd_values), axis=0)

        return ef_values, ef_norm_values, pf_values, pf_norm_values, rthd_values, rthd_norm_values

    def increase_stress(self):
        self.real_time_healt_data.increase_stress()

    def decrease_stress(self):
        self.real_time_healt_data.decrease_stress()

    def adjust_variables(self):
        self.real_time_healt_data.adjust_variables()

    def adjust_stress(self, stress_level_change):
        logger.debug(f'Stress Level Change {stress_level_change}')
        if stress_level_change > 0:
            for _ in range(abs(stress_level_change)):
                self.increase_stress()
        elif stress_level_change < 0:
            for _ in range(abs(stress_level_change)):
                self.decrease_stress()
        else:
            self.adjust_variables()

    def frame_step(self, amount=1):
        ef_values, ef_norm_values, pf_values, pf_norm_values, rthd_values, rthd_norm_values = self.get_readings(
            amount=amount)
        # readings = np.concatenate((ef_values, pf_values, rthd_values), axis=0)

        # ef_norm_values_mean = np.mean(ef_norm_values, axis=1)
        # pf_norm_values_mean = np.mean(pf_norm_values, axis=1)
        rthd_norm_values_mean = np.mean(rthd_norm_values, axis=1)
        state = np.concatenate((ef_norm_values, pf_norm_values, rthd_norm_values_mean), axis=0)

        reward = np.dot(self.weights, state)

        return reward, state, ef_values, pf_values, rthd_values


if __name__ == '__main__':
    from .ExternalFactors import Environment
    from .PhysiologicalFingerprint import Persona
    from .RealTimeHealthData import Chest

    michael = Persona(age=28, gender='male', resting_heart_rate=55)
    stefan = Persona(age=22, gender='male', resting_heart_rate=70)
    cold_weather = Environment(env_type='extreme', temperature=-14, location='Mountains', altitude=2250)
    # chest = Chest(michael.age, michael.gender, michael.resting_heart_rate)
    chest = Chest()
    test_env = SimulationEnviornment(external_factor=cold_weather,
                                     physiological_fingerprint=michael,
                                     real_time_health_data=chest)
    test_env.get_readings(10)
    test_env.physiological_fingerprint = stefan

    for _ in range(5):
        test_env.increase_stress()

    test_env.get_readings(10)
    test_env.get_readings(15)

    # print(np.mean(values, axis=1))
    # import matplotlib.pyplot as plt
    # x = Chest(28, 'male', 61)
    # values = x.get_values()
    #
    # fig, ax = plt.subplots(len(values), sharex=False)
    # for index, value in enumerate(values):
    #     ax[index].plot(value)
    #     ax[index].title.set_text(x.sensors[index].__class__)
    # plt.show()

