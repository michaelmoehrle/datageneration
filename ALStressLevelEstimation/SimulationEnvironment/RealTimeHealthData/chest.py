import numpy as np
from interface import implements

from .realTimeHealthDataInterface import RealTimeHealthDataInterface
from .sensors import *

import logging
logger = logging.getLogger(__name__)


class Chest(implements(RealTimeHealthDataInterface)):
    def __init__(self, age=None, gender=None, resting_heart_rate=None):
        self.breathing_frequency = BreathingFrequency(minimum=6, normal=10, maximum=35, step=1, deviation=0.2, poss_deviations=[0.2, 1, 2, 3])
        self.core_body_temperature = CoreBodyTemperature(minimum=32, normal=36.7, maximum=42, step=0.5, deviation=0.2, poss_deviations=[0.2, 0.4, 0.6, 0.7])
        self.hand_right_temperature = ExtremitySkinTemperature(minimum=27, normal=None, maximum=39, step=0.1,
                                                               deviation=0.2, poss_deviations=[0.2, 0.4])
        self.hand_left_temperature = ExtremitySkinTemperature(minimum=27, normal=None, maximum=39, step=0.1,
                                                              deviation=0.2, poss_deviations=[0.2, 0.4])
        self.foot_right_temperature = ExtremitySkinTemperature(minimum=25, normal=None, maximum=39, step=0.1,
                                                               deviation=0.2, poss_deviations=[0.2, 0.4])
        self.foot_left_temperature = ExtremitySkinTemperature(minimum=25, normal=None, maximum=39, step=0.1,
                                                              deviation=0.2, poss_deviations=[0.2, 0.4])
        self.heart_rate = HeartRate(minimum=40, normal=70, maximum=220, step=1, deviation=5, age=age,
                                    resting_heart_rate=resting_heart_rate, poss_deviations=[5, 7, 10, 15])
        self.heart_rate_variability = HeartRateVariability(minimum=15, normal=39, maximum=39, step=1, deviation=1,
                                                           age=None, gender=None, poss_deviations=[1, 2, 3])
        self.humidity = Humidity(minimum=0, normal=55, maximum=100, step=1, deviation=1, poss_deviations=[1, 2])

    def generate_values(self, amount: int):
        result = np.empty((0, amount))
        norm_result = np.empty((0, amount))
        for sensor in self.__dict__.values():
            values, norm_values = sensor.generate_values(amount=amount)
            result = np.append(result, [values], axis=0)
            norm_result = np.append(norm_result, [norm_values], axis=0)
            logger.debug(f"{sensor.__class__}: {values}")

        return result, norm_result

    def update_persona_info(self, age: int, gender: str, resting_heart_rate: int):
        logger.debug(f"{self.__class__} update persona_info")
        self.heart_rate.update_persona_info(age, resting_heart_rate)
        self.heart_rate_variability.update_persona_info(age, gender)

    def increase_stress(self):
        for sensor in self.__dict__.values():
            logger.debug(f'Increase Sensor: {sensor}')

            sensor.increase_stress()

    def decrease_stress(self):
        for sensor in self.__dict__.values():
            sensor.decrease_stress()

    def adjust_variables(self):
        for sensor in self.__dict__.values():
            sensor.adjust_variables()
