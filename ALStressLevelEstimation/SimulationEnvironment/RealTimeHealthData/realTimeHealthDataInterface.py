from interface import Interface


class RealTimeHealthDataInterface(Interface):
    def generate_values(self, amount: int):
        pass

    def update_persona_info(self, age: int, gender: str, resting_heart_rate: int) -> None:
        pass

    def increase_stress(self):
        pass

    def decrease_stress(self):
        pass

    def adjust_variables(self):
        pass
