from .basic_sensor import BasicSensor
import numpy as np


class BreathingFrequency(BasicSensor):
    def increase_stress(self):
        step = (self.max - self.min) / 10
        self.norm += step
        self.deviation = np.random.choice(self._possible_deviations)
        # print(f'{self.__class__}, STep: {step}, Mean: {self.norm}')

    def decrease_stress(self):
        step = (self.max - self.min) / 10
        self.norm -= step
        self.deviation = np.random.choice(self._possible_deviations)

    def adjust_variables(self):
        self.deviation = np.random.choice(self._possible_deviations)
