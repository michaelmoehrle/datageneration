from abc import ABC

import numpy as np
from scipy import stats

import logging
logger = logging.getLogger(__name__)


class BasicSensor(ABC):
    def __init__(self, minimum, normal, maximum, step, deviation, poss_deviations):
        self._min = minimum
        self._norm = normal
        self._max = maximum
        self._step = step
        self._deviation = deviation
        self._possible_deviations = poss_deviations

        self._values = None
        self._dist = None
        self._pdf = None
        self._prob = None

    @property
    def min(self):
        return self._min

    @property
    def norm(self):
        if self._norm is None:
            self._norm = (self.min + self.max) / 2
        return self._norm

    @norm.setter
    def norm(self, value):
        self._norm = value
        self.reset_properties()

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, value):
        logger.debug(f"{self.__class__} update max value")
        self._max = value
        self.reset_properties()

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, value):
        self._step = value
        self.reset_properties()

    @property
    def deviation(self):
        return self._deviation

    @deviation.setter
    def deviation(self, value):
        self._deviation = value
        self.reset_properties()

    @property
    def values(self):
        if self._values is None:
            logger.debug("calculated values")
            self._values = np.arange(start=self.min, stop=self.max + self.step / 2, step=self.step)
        return self._values

    @property
    def dist(self):
        if self._dist is None:
            logger.debug("calculate dist")
            self._dist = stats.truncnorm(
                (self.min - self.norm) / self.deviation,
                (self.max - self.norm) / self.deviation, loc=self.norm, scale=self.deviation)
        return self._dist

    @property
    def pdf(self):
        if self._pdf is None:
            logger.debug("calculate pdf")
            self._pdf = self.dist.pdf(self.values)
        return self._pdf

    @property
    def prob(self):
        if self._prob is None:
            logger.debug("calculate prob")
            self._prob = self.pdf / np.sum(self.pdf)
        return self._prob

    def reset_properties(self):
        logger.debug(f"{self.__class__} reset properties")
        self._values = None
        self._dist = None
        self._pdf = None
        self._prob = None

    def generate_values(self, amount):
        result = np.random.choice(self.values, amount, p=self.prob)
        norm_result = self.normalise_array(result)
        return result, norm_result

    def normalise_array(self, array):
        norm_result = (array - self.min)/(self.max - self.min)
        return norm_result

    def increase_stress(self):
        pass

    def decrease_stress(self):
        pass

    def adjust_variables(self):
        pass


if __name__ == '__main__':
    test = BasicSensor(10, 15, 30, 1, 1)
    print(test.generate_values(10))
    print(test.generate_values(10))
    print(test.max)
    test.max = 25
    print(test.max)
    print(test.generate_values(10))
