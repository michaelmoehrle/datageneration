from .basic_sensor import BasicSensor
import numpy as np


class Humidity(BasicSensor):
    def increase_stress(self):
        step = (self.max - self.min) / 18
        self.norm += step
        self.deviation = np.random.choice(self._possible_deviations)

    def decrease_stress(self):
        step = (self.max - self.min) / 18
        self.norm -= step
        self.deviation = np.random.choice(self._possible_deviations)

    def adjust_variables(self):
        self.deviation = np.random.choice(self._possible_deviations)
