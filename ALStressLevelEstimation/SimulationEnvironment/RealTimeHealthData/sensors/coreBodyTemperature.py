from .basic_sensor import BasicSensor
import numpy as np


class CoreBodyTemperature(BasicSensor):
    def increase_stress(self):
        self.norm = np.random.choice(np.array([33, 35.5, 36.7, 38, 41]), p=[0.1, 0.2, 0.4, 0.2, 0.1])
        # p=[0.05, 0.1, 0.4, 0.3, 0.15]
        self.deviation = np.random.choice(self._possible_deviations)

    def decrease_stress(self):
        self.increase_stress()

    def adjust_variables(self):
        self.increase_stress()
