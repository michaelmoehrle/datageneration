from .basic_sensor import BasicSensor
import numpy as np


class ExtremitySkinTemperature(BasicSensor):
    def increase_stress(self):
        self.norm = np.random.choice(np.linspace(self.min, self.max, num=5), p=[0.1, 0.2, 0.4, 0.2, 0.1])
        self.deviation = np.random.choice(self._possible_deviations)

    def decrease_stress(self):
        self.increase_stress()

    def adjust_variables(self):
        self.increase_stress()
