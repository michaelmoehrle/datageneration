import logging

from .basic_sensor import BasicSensor
import numpy as np

logger = logging.getLogger(__name__)


class HeartRate(BasicSensor):
    def __init__(self, minimum, normal, maximum, step, deviation, age, resting_heart_rate, poss_deviations):
        super().__init__(minimum=minimum, normal=resting_heart_rate if resting_heart_rate else normal,
                         maximum=maximum - age if age else maximum, step=step,
                         deviation=deviation, poss_deviations=poss_deviations)

        self.max_max = maximum  # Unmodified maximum heart rate

    def update_persona_info(self, age, resting_heart_rate):
        logger.debug(f"{self.__class__} update persona_info")
        self.max = self.max_max - age
        self.norm = resting_heart_rate

    def increase_stress(self):
        step = (self.max_max - self.min) / 10
        self.norm += step
        self.deviation = np.random.choice(self._possible_deviations)
        logger.debug(f'{self.__class__}, STep: {step}, Mean: {self.norm}, SD: {self.deviation}')

    def decrease_stress(self):
        step = (self.max_max - self.min) / 10
        self.norm -= step
        self.deviation = np.random.choice(self._possible_deviations)

    def adjust_variables(self):
        self.deviation = np.random.choice(self._possible_deviations)
