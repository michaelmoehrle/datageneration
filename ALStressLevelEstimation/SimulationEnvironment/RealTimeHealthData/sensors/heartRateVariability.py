import logging

from range_key_dict import RangeKeyDict

from .basic_sensor import BasicSensor

import numpy as np

logger = logging.getLogger(__name__)

# Values taken from https://elitehrv.com/normal-heart-rate-variability-age-gender
RMSSD_AGE_GENDER_DICT = RangeKeyDict({
    (0, 24): {'male': 86.5, 'female': 68.7},
    (25, 34): {'male': 66.0, 'female': 55.7},
    (35, 44): {'male': 50.4, 'female': 45.6},
    (45, 54): {'male': 39.6, 'female': 41.7},
    (55, 64): {'male': 32.1, 'female': 32.5},
    (65, 74): {'male': 30.6, 'female': 24.8},
    (75, 120): {'male': 33.1, 'female': 25.5}
})


class HeartRateVariability(BasicSensor):
    def __init__(self, minimum, normal, maximum, step, deviation, age, gender, poss_deviations):
        super().__init__(minimum=minimum, normal=RMSSD_AGE_GENDER_DICT[age][gender] if age and gender else normal,
                         maximum=maximum, step=step, deviation=deviation, poss_deviations=poss_deviations)

    def update_persona_info(self, age, gender):
        logger.debug(f"{self.__class__} update persona_info")
        self.norm = RMSSD_AGE_GENDER_DICT[age][gender]

    def increase_stress(self):
        step = (self.max - self.min) / 8
        self.norm -= step
        self.deviation = np.random.choice(self._possible_deviations)

    def decrease_stress(self):
        step = (self.max - self.min) / 8
        self.norm += step
        self.deviation = np.random.choice(self._possible_deviations)

    def adjust_variables(self):
        self.deviation = np.random.choice(self._possible_deviations)
