import numpy as np
from interface import implements
from collections import namedtuple

from .physiologicalFingerprintInterface import PhysiologicalFingerprintInterface

GENDER_DICT = {'male': 0, 'female': 1, 'diverse': 2}

Age = namedtuple('Age', 'value min max')
Age.__new__.__defaults__ = (None, 1, 100)
Gender = namedtuple('Gender', 'value min max')
Gender.__new__.__defaults__ = (None, 0, 2)
RestingHeartRate = namedtuple('RestingHeartRate', 'value min max')
RestingHeartRate.__new__.__defaults__ = (None, 30, 100)


class Persona(implements(PhysiologicalFingerprintInterface)):
    def __init__(self, age, gender, resting_heart_rate):
        self._age = Age(age)
        self._gender = Gender(GENDER_DICT[gender])
        self._resting_heart_rate = RestingHeartRate(resting_heart_rate)

    @property
    def age(self):
        return self._age.value

    @property
    def gender(self):
        return self._gender.value

    @property
    def resting_heart_rate(self):
        return self._resting_heart_rate.value

    def get_values(self):
        result = np.array([self.age, self.gender, self.resting_heart_rate])
        norm_result = np.array([self.normalise_value(self.age, self._age.min, self._age.max),
                                self.normalise_value(self.gender, self._gender.min, self._gender.max),
                                self.normalise_value(self.resting_heart_rate, self._resting_heart_rate.min, self._resting_heart_rate.max)])

        return result, norm_result

    def normalise_value(self, value, minimum, maximum):
        norm_result = (value - minimum)/(maximum - minimum)
        return norm_result
