from interface import Interface


class PhysiologicalFingerprintInterface(Interface):
    @property
    def age(self):
        pass

    @property
    def gender(self):
        pass

    @property
    def resting_heart_rate(self):
        pass

    def get_values(self):
        pass
